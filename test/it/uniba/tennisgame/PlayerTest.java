package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void scoreShouldBeIncreased() {
		//Arrange
		Player giocatore = new Player("Federer", 0);
		
		
		//Act
		giocatore.incrementScore();
		
		//Assert
		assertEquals(1, giocatore.getScore());
	}
	
	@Test
	public void scoreShouldNotBeIncreased() {
		//Arrange
		Player giocatore = new Player("Federer", 0);
		
		//Assert
		assertEquals(0, giocatore.getScore());
	}
	
	@Test
	public void scoreShouldBeLove() {
		//Arrange
		Player giocatore = new Player("Federer", 0);
		//Act
		String scoreAsString = giocatore.getScoreAsString();
		//assert
		assertEquals("love", scoreAsString);
	}
	
	@Test
	public void scoreShouldBeFifteen() {
		//Arrange
		Player giocatore = new Player("Federer", 1);
		//Act
		String scoreAsString = giocatore.getScoreAsString();
		//Assert
		assertEquals("fifteen", scoreAsString);
	}
	
	@Test
	public void scoreShouldBeThirty() {
		//Arrange
		Player giocatore = new Player("Federer", 2);
		//Act
		String scoreAsString = giocatore.getScoreAsString();
		//Assert
		assertEquals("thirty", scoreAsString);
	}
	
	@Test
	public void scoreShouldBeForty() {
		//Arrange
		Player giocatore = new Player("Federer", 3);
		//Act
		String scoreAsString = giocatore.getScoreAsString();
		//Assert
		assertEquals("forty", scoreAsString);
	}
	
	@Test
	public void scoreShouldNullIfNegative() {
		//Arrange
		Player giocatore = new Player("Federer", -1);
		//Act
		String scoreAsString = giocatore.getScoreAsString();
		//Assert
		assertNull(scoreAsString);
	}
	
	@Test
	public void scoreShouldNullIfMoreThanThree() {
		//Arrange
		Player giocatore = new Player("Federer", 4);
		//Act
		String scoreAsString = giocatore.getScoreAsString();
		//Assert
		assertNull(scoreAsString);
	}
	
	@Test
	public void shouldBeTie() {
		//Arrange
		Player giocatore = new Player("Federer", 2);
		Player opponent = new Player("Nadal", 2);
		//Act
		boolean tie = giocatore.isTieWith(opponent);
		//Assert
		assertTrue(tie);
	}
	
	@Test
	public void shouldNotBeTie() {
		//Arrange
		Player giocatore = new Player("Federer", 3);
		Player opponent = new Player("Nadal", 2);
		//Act
		boolean tie = giocatore.isTieWith(opponent);
		//Assert
		assertFalse(tie);
	}
	
	@Test
	public void shouldHaveAtLeastFortyPoints() {
		//Arrange
		Player giocatore = new Player("Federer", 3);
		//Act
		boolean outcome = giocatore.hasAtLeastFortyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveAtLeastFortyPoints() {
		//Arrange
		Player giocatore = new Player("Federer", 2);
		//Act
		boolean outcome = giocatore.hasAtLeastFortyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveLessThanFortyPoints() {
		//Arrange
		Player giocatore = new Player("Federer", 2);
		//Act
		boolean outcome = giocatore.hasLessThanFortyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveLessThanFortyPoints() {
		//Arrange
		Player giocatore = new Player("Federer", 3);
		//Act
		boolean outcome = giocatore.hasLessThanFortyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveMoreThanFortyPoints() {
		//Arrange
		Player giocatore = new Player("Federer", 4);
		//Act
		boolean outcome = giocatore.hasMoreThanFourtyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveMoreThanFortyPoints() {
		//Arrange
		Player giocatore = new Player("Federer", 3);
		//Act
		boolean outcome = giocatore.hasMoreThanFourtyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	
	@Test
	public void shouldHaveOnePointAdvantage() {
		//Arrange
		Player giocatore = new Player("Federer", 4);
		Player giocatore2 = new Player("Nadaol",3);
		
		//Act
		boolean advance = giocatore.hasOnePointAdvantageOn(giocatore2);
		// Assert
		assertTrue(advance);
	}
	
	@Test
	public void shouldNotHaveOnePointAdvantage() {
		//Arrange
		Player giocatore = new Player("Federer", 3);
		Player giocatore2 = new Player("Nadaol",3);
		
		//Act
		boolean advance = giocatore.hasOnePointAdvantageOn(giocatore2);
		// Assert
		assertFalse(advance);
	}
}
