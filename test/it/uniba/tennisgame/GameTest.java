package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void testFifteenThirty() throws Exception{
		//Arrange
			Game game = new Game("Federer", "Nadal");
			String playerName1 = game.getPlayerName1();
			String playerName2 = game.getPlayerName2();
		//Act
			game.incrementPlayerScore(playerName1);
			
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			
			String status = game.getGameStatus();
		//Assert
			assertEquals("Federer fifteen - Nadal thirty",status);
			
			
	}
	
	@Test
	public void testFortyThirty() throws Exception{
		//Arrange
			Game game = new Game("Federer", "Nadal");
			String playerName1 = game.getPlayerName1();
			String playerName2 = game.getPlayerName2();
		//Act
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			
			String status = game.getGameStatus();
		//Assert
			assertEquals("Federer forty - Nadal thirty",status);
			
			
	}
	
	@Test
	public void testPlayer1Wins() throws Exception{
		//Arrange
			Game game = new Game("Federer", "Nadal");
			String playerName1 = game.getPlayerName1();
			String playerName2 = game.getPlayerName2();
		//Act
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			
			
			
			String status = game.getGameStatus();
		//Assert
			assertEquals("Federer wins",status);
			
			
	}

	@Test
	public void testfifteenForty() throws Exception{
		//Arrange
			Game game = new Game("Federer", "Nadal");
			String playerName1 = game.getPlayerName1();
			String playerName2 = game.getPlayerName2();
		//Act
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			
			game.incrementPlayerScore(playerName1);
			
			
			
			
			String status = game.getGameStatus();
		//Assert
			assertEquals("Federer fifteen - Nadal forty",status);
			
			
	}
	
	@Test
	public void testDeuce() throws Exception{
		//Arrange
			Game game = new Game("Federer", "Nadal");
			String playerName1 = game.getPlayerName1();
			String playerName2 = game.getPlayerName2();
		//Act
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			
			
			
			
			String status = game.getGameStatus();
		//Assert
			assertEquals("Deuce",status);
			
			
	}

	@Test
	public void testAdvantagePlayer1() throws Exception{
		//Arrange
			Game game = new Game("Federer", "Nadal");
			String playerName1 = game.getPlayerName1();
			String playerName2 = game.getPlayerName2();
		//Act
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			
			
			
			
			String status = game.getGameStatus();
		//Assert
			assertEquals("Advantage Federer",status);
		}
	

	@Test
	public void testPlayer2Wins() throws Exception{
		//Arrange
			Game game = new Game("Federer", "Nadal");
			String playerName1 = game.getPlayerName1();
			String playerName2 = game.getPlayerName2();
		//Act
			game.incrementPlayerScore(playerName1);
			
			
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			
			
			
			
			
			
			
			String status = game.getGameStatus();
		//Assert
			assertEquals("Nadal wins",status);
		}
	
	@Test
	public void testAdvantagePlayer2() throws Exception{
		//Arrange
			Game game = new Game("Federer", "Nadal");
			String playerName1 = game.getPlayerName1();
			String playerName2 = game.getPlayerName2();
		//Act
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			
			String status = game.getGameStatus();
		//Assert
			assertEquals("Advantage Nadal",status);
		}
	
	
	@Test
	public void testDeuceAfterAdvantage() throws Exception{
		//Arrange
			Game game = new Game("Federer", "Nadal");
			String playerName1 = game.getPlayerName1();
			String playerName2 = game.getPlayerName2();
		//Act
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName1);
			
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName2);
			game.incrementPlayerScore(playerName1);
			game.incrementPlayerScore(playerName2);
			
			String status = game.getGameStatus();
		//Assert
			assertEquals("Deuce",status);
		}

}

/**
 * The Game class represents a tennis game. This class should be used as
 * follows:
 * 
 * // Create a new game: 
 * Game game = new Game("name1", "name2");
 *
 * // Get the name of the two players: 
 * String playerName1 = game.getPlayerName1(); 
 * String playerName2 = game.getPlayerName2();
 *
 * // Update the game by incrementing the score of either the first or second
 * player: 
 * game.incrementPlayerScore(playerName1);
 * game.incrementPlayerScore(playerName2);
 *
 * // Get the status of the game: 
 * String status = game.getGameStatus();
 * 
 * See the README.md file to understand the rules behind the game status returned by tennis_game
 * 
 */